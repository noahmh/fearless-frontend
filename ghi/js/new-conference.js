window.addEventListener('DOMContentLoaded', async () => {
    const locationURL = "http://localhost:8000/api/locations/"
    const locationResponse = await fetch(locationURL);

    const locationDropDown = document.getElementById("location")

    if (locationResponse.ok) {
        const locationData = await locationResponse.json();

        for (const location of locationData.locations){
            const option = document.createElement("option");
            option.innerHTML = location.name;
            option.value = location.id;
            locationDropDown.appendChild(option)
            console.log(location)
        }
    }
    const formTag = document.getElementById("create-conference-form");
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        let dataObject = Object.fromEntries(formData);

        let startDate = new Date(dataObject.starts)
        let startISO = startDate.toISOString();
        let endDate = new Date(dataObject.ends)
        let endISO = endDate.toISOString();
        dataObject.starts = startISO;
        dataObject.ends = endISO;

        const dataObjectJSON = JSON.stringify(dataObject)

        console.log(dataObject)
        console.log(dataObjectJSON)

        const conferenceURL = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
            method: 'post',
            body: dataObjectJSON,
            headers: {
                'Content-type': 'applications/json',
            },
        }
        const response = await fetch(conferenceURL, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference);
        }
    });
});
