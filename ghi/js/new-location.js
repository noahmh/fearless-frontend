function stateHTMLAdd(state) {
    return `
    <option>${state.name}</option>
    `
}
const stateDropDown = document.getElementById("state")

window.addEventListener('DOMContentLoaded', async () => {
    const stateURL = "http://localhost:8000/api/states/";
    const stateResponse = await fetch(stateURL);

    if (stateResponse.ok) {
        const stateData = await stateResponse.json();

        for (const state of stateData.states){
            const option = document.createElement('option');
            option.innerHTML = state.name;
            option.value = state.abbreviation;
            stateDropDown.appendChild(option)
        }
        console.log(stateData)
    }
    const formTag = document.getElementById("create-location-form");
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const dataObject = JSON.stringify(Object.fromEntries(formData));
        console.log(dataObject)

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: dataObject,
            headers: {
                'Content-Type': 'application/json',
        },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
        }
    });
});






/*
Location data format
{
  "name": "Moscone Center",
  "city": "San Francisco",
  "room_count": 62,
  "state": "CA"
}
*/

// const locationURL = "http://localhost:8000/api/locations/";
// const fetchConfig = {
//     method: "post",
//     body: "json",
//     headers: {
//         'Content-Type': 'application/json',
//     },
// };
// const response = await fetch(locationURL, fetchConfig);
// if (response.ok) {
//     formTag.reset();
//     const newLocation = await response.json();
//     console.log(newLocation)
// }
