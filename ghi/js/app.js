function createCard(title, description, pictureUrl,location, start_date, end_date) {
    return `
    <div class="col p-3">
      <div class="card shadow-lg">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-header">
          <p class="text-secondary">${location}
        </div>
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
          <p>${start_date} / ${end_date}</p>
        </div>
      </div>
    </div>
    `;
  }


function dateFixer(date){
  let month = date.slice(8,10);
  let day = date.slice(5,7);
  let year = date.slice(0, 4);
  return `${day}/${month}/${year}`;
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    console.log(response);

    if (!response.ok) {
        console.error("there was an error, you bitch")
    } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const title = details.conference.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const location = details.conference.location.name;
              const date_start = dateFixer(details.conference.starts);
              const date_end = dateFixer(details.conference.ends);

              const html = createCard(title, description, pictureUrl, location, date_start, date_end);

              const column = document.querySelector('.row');
              column.innerHTML += html;

              console.log(details)
            }
          }

    }
  });

//   window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';

//     try {
//       const response = await fetch(url);

//       if (!response.ok) {
//         // Figure out what to do when the response is bad
//       } else {
//         const data = await response.json();
//       }
//     } catch (e) {
//       // Figure out what to do if an error is raised
//     }

//   });
