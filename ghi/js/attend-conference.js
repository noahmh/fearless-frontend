window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
      let loadingIcon = document.getElementById("loading-conference-spinner")
      loadingIcon.classList.add("d-none")
      selectTag.classList.remove("d-none")
    }
    let formTag = document.getElementById("create-attendee-form")
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        let dataObject = Object.fromEntries(formData);
        const dataObjectJSON = JSON.stringify(dataObject)

        const attendeesURL = "http://localhost:8001/api/attendees/";
        const fetchConfig = {
            method: 'post',
            body: dataObjectJSON,
            headers: {
                'Content-type': 'applications/json',
            },
        }
        console.log(dataObject)
        const response = await fetch(attendeesURL, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newAttendee = await response.json();

            formTag.classList.add("d-none")
            let successMessage = document.getElementById("success-message")
            successMessage.classList.remove("d-none")
        }
    });
  });
