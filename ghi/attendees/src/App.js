import Nav from './Nav';

function App(props) {
  if (props.attendees == undefined) {
    return null;
  }
  return (
    <>
    <Nav />
    <div className="container-fluid border border-dark-subtle shadow">
      <table className="table table-hover table-secondary table-striped border border-dark-subtle shadow container-fluid mt-5">
        <thead className="table-group-divider">
          <tr>
            <th>Name</th>
            <th>Conference</th>
          </tr>
        </thead>
        <tbody className="border-top border-dark-subtle">
          {props.attendees.map(attendee => {
            return (
            <tr className="object-fit" key={attendee.href }>
              <td>{ attendee.name }</td>
              <td>{ attendee.conference }</td>
            </tr>
          );
          })}
        </tbody>
      </table>
    </div>
    </>
  );
}

export default App;
